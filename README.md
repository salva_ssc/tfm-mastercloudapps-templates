# TFM-MasterCloudApps-Templates

Proyecto de plantillas Gitlab para proyectos Java, que facilita la creación de pipelines utilizando la importación de plantillas ya existentes.

Para la realización de estas plantillas se ha usado el modelo de desarrollo *Git Flow*, en el que se ejecutarán los siguientes jobs en las siguientes ramas:

* **feature/**: build, unitary_test, sonarqube


* **develop:** build, unitary_test, selenium_test, rest_test, sonarqube


* **nightly:** build, unitary_test, selenium_test, rest_test, security, validate_and_build_docker_images, push_images, owasp_scan


* **master:** build, unitary_test, selenium_test, rest_test, security


* **release/:** build, unitary_test, selenium_test, rest_test, security, validate_and_build_docker_images, push_images, owasp_scan, deploy_pre, performance


* **vX.x.x:** deploy_pro

Se pueden ver ejemplos de utilización de estas plantillas en los siguientes proyectos:

- https://gitlab.com/salva_ssc/tfm-mastercloudapps-anuncios
- https://gitlab.com/salva_ssc/tfm-mastercloudapps-items